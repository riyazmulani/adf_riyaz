# Databricks notebook source
dbutils.secrets.help()

# COMMAND ----------

dbutils.secrets.listScopes()

# COMMAND ----------

dbutils.secrets.list(scope = "formulaone1-scope")

# COMMAND ----------

dbutils.secrets.get(scope = "formulaone1-scope", key = "formulaone1-account-key")

# Databricks notebook source
# MAGIC %md
# MAGIC Access ADLS using Access Key
# MAGIC 1.set the spark conf fs.azure.account.key
# MAGIC 2.list files from demo container
# MAGIC 3.read data from emp.csv

# COMMAND ----------

formulaone1_account_key = dbutils.secrets.get(scope = "formulaone1-scope", key = "formulaone1-account-key")

# COMMAND ----------

spark.conf.set(
    "fs.azure.account.key.bwtformulaone1.dfs.core.windows.net",
    formulaone1_account_key)

# COMMAND ----------

display(dbutils.fs.ls("abfss://test@bwtformulaone1.dfs.core.windows.net/input_data"))

# COMMAND ----------

display(spark.read.csv("abfss://test@bwtformulaone1.dfs.core.windows.net/input_data/emp.csv"))

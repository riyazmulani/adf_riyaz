# Databricks notebook source
# MAGIC %md
# MAGIC Access ADLS using service principle
# MAGIC 1.Register Azure AD Application/Service principle
# MAGIC 2.Generate a secret password/secretfor the appl
# MAGIC 3.set a spark config with appl/clientid,directory/ tenanat id secret
# MAGIC 4.assign role storage blob contributor to the Data lake

# COMMAND ----------

client_id = dbutils.secrets.get(scope="formulaone1-scope",key="formulaone1-app-client-id")
tenant_id =dbutils.secrets.get(scope="formulaone1-scope",key="formulaone1-tenant-id")
client_secret = dbutils.secrets.get(scope="formulaone1-scope",key="formulaone1-app-client-secret")

# COMMAND ----------


spark.conf.set("fs.azure.account.auth.type.bwtformulaone1.dfs.core.windows.net", "OAuth")
spark.conf.set("fs.azure.account.oauth.provider.type.bwtformulaone1.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider")
spark.conf.set("fs.azure.account.oauth2.client.id.bwtformulaone1.dfs.core.windows.net", client_id)
spark.conf.set("fs.azure.account.oauth2.client.secret.bwtformulaone1.dfs.core.windows.net", client_secret)
spark.conf.set("fs.azure.account.oauth2.client.endpoint.bwtformulaone1.dfs.core.windows.net", f"https://login.microsoftonline.com/{tenant_id}/oauth2/token")

# COMMAND ----------

display(dbutils.fs.ls("abfss://test@bwtformulaone1.dfs.core.windows.net/input_data"))

# COMMAND ----------

display(spark.read.csv("abfss://test@bwtformulaone1.dfs.core.windows.net/input_data/emp.csv"))

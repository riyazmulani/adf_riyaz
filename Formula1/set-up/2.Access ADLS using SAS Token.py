# Databricks notebook source
# MAGIC %md
# MAGIC Access ADLS using SAS Token
# MAGIC 1.set the spark conf sas token
# MAGIC 2.list files from demo container
# MAGIC 3.read data from emp.csv

# COMMAND ----------

formulaone_sas_token = dbutils.secrets.get(scope="formulaone1-scope",key= "formulaone1-sas-token")

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.bwtformulaone1.dfs.core.windows.net", "SAS")
spark.conf.set("fs.azure.sas.token.provider.type.bwtformulaone1.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.sas.FixedSASTokenProvider")
spark.conf.set("fs.azure.sas.fixed.token.bwtformulaone1.dfs.core.windows.net",formulaone_sas_token)

# COMMAND ----------

display(dbutils.fs.ls("abfss://test@bwtformulaone1.dfs.core.windows.net/input_data"))

# COMMAND ----------

display(spark.read.csv("abfss://test@bwtformulaone1.dfs.core.windows.net/input_data/emp.csv"))

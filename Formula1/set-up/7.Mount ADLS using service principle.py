# Databricks notebook source
# MAGIC %md
# MAGIC mount Azure Data lake  using service principle
# MAGIC 1.et client-id,tenant-id,client-secret from key vault
# MAGIC 2.set spark conf with App/client-id,directory/tenant-id, secreets
# MAGIC 3.call file system utiliti mount to mout the storage
# MAGIC 4.explore other file system related to mount

# COMMAND ----------

client_id = dbutils.secrets.get(scope="formulaone1-scope",key="formulaone1-app-client-id")
tenant_id =dbutils.secrets.get(scope="formulaone1-scope",key="formulaone1-tenant-id")
client_secret = dbutils.secrets.get(scope="formulaone1-scope",key="formulaone1-app-client-secret")

# COMMAND ----------

configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret": client_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tenant_id}/oauth2/token"}

# COMMAND ----------

dbutils.fs.mount(
  source = "abfss://test@bwtformulaone1.dfs.core.windows.net/",
  mount_point = "/mnt/bwtformulaone1/test",
  extra_configs = configs)

# COMMAND ----------

display(dbutils.fs.ls("/mnt/bwtformulaone1/test/input_data"))

# COMMAND ----------

display(spark.read.csv("dbfs:/mnt/bwtformulaone1/test/input_data/emp.csv"))

# COMMAND ----------

display(dbutils.fs.mounts())

# COMMAND ----------



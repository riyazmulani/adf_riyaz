# Databricks notebook source
# MAGIC %md
# MAGIC Explore DBFS ROOT
# MAGIC list all the folder in dbfs root
# MAGIC interact with dbfs browser
# MAGIC upload file to dbfs root

# COMMAND ----------

display(dbutils.fs.ls("/"))

# COMMAND ----------

display(dbutils.fs.ls("/FileStore"))

# COMMAND ----------

display(spark.read.csv("/FileStore/emp.csv"))

# COMMAND ----------



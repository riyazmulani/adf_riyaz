# Databricks notebook source
# MAGIC %md
# MAGIC ### Step-1 Read the lap_time.csv file

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, IntegerType, StringType,DateType,FloatType

# COMMAND ----------

lap_times_schema = StructType(fields=[StructField("raceId",IntegerType(),False),
                                   StructField("driverId",IntegerType(),True),
                                   StructField("position",StringType(),True),
                                   StructField("lap",IntegerType(),True),
                                   StructField("time",StringType(),True),
                                   StructField("milliseconds",IntegerType(),True)


                                   ])

# COMMAND ----------

lap_times_df = spark.read.csv("/mnt/bwtformulaone1/raw/lap_times",schema = lap_times_schema)

# COMMAND ----------

display(lap_times_df)

# COMMAND ----------

lap_times_df.count()

# COMMAND ----------

# MAGIC %md
# MAGIC ###Rename colum and add new colum
# MAGIC 1.columname
# MAGIC 2.ingestion date
# MAGIC 3concanite name and surname

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,col

# COMMAND ----------

final_lap_times_df = lap_times_df.withColumnRenamed("driverId","driver_id") \
                                  .withColumnRenamed("raceId","race_id") \
                                  .withColumn("ingestion_date", current_timestamp()) 


# COMMAND ----------

display(final_lap_times_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ##write output to processed container in parquet format

# COMMAND ----------

final_lap_times_df.write.mode("overwrite").parquet("/mnt/bwtformulaone1/processed/lap_times")

# COMMAND ----------

display(spark.read.parquet("/mnt/bwtformulaone1/processed/lap_times"))

# COMMAND ----------



# Databricks notebook source
# MAGIC %md
# MAGIC ### Step-1 Read the qualifying.json file

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, IntegerType, StringType,DateType,FloatType

# COMMAND ----------

qualifying_schema = StructType(fields=[StructField("qualifyingId",IntegerType(),False),
                                   StructField("raceId",IntegerType(),True),      
                                   StructField("driverId",IntegerType(),True),
                                   StructField("constructorId",IntegerType(),True),
                                   StructField("number",IntegerType(),True),
                                   StructField("position",IntegerType(),True),
                                   StructField("q1",StringType(),True),
                                   StructField("q2",StringType(),True),
                                   StructField("q3",StringType(),True)


                                   ])

# COMMAND ----------

qualifying_df = spark.read.json("/mnt/bwtformulaone1/raw/qualifying",schema = qualifying_schema,multiLine = True)

# COMMAND ----------

display(qualifying_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ###Rename colum and add new colum
# MAGIC 1.columname
# MAGIC 2.ingestion date
# MAGIC 3concanite name and surname

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,col

# COMMAND ----------

final_qualifying_df = qualifying_df.withColumnRenamed("driverId","driver_id") \
                                  .withColumnRenamed("raceId","race_id") \
                                  .withColumnRenamed("qualifyingId","qualifying_id") \
                                  .withColumnRenamed("constructorId","constructor_id") \
                                  .withColumn("ingestion_date", current_timestamp()) 


# COMMAND ----------

display(final_qualifying_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ##write output to processed container in parquet format

# COMMAND ----------

final_qualifying_df.write.mode("overwrite").parquet("/mnt/bwtformulaone1/processed/qualifying")

# COMMAND ----------

display(spark.read.parquet("/mnt/bwtformulaone1/processed/qualifying"))

# COMMAND ----------



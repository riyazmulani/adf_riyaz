# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest constructors.json file

# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-1 Read the json file using spark datatframr reader

# COMMAND ----------

constructors_schema = "constructorId INT, constructorRef STRING, name STRING, nationality STRING, URL STRING"

# COMMAND ----------

constructor_df = spark.read.json("/mnt/bwtformulaone1/raw/constructors.json",schema=constructors_schema)

# COMMAND ----------

constructor_df.display()

# COMMAND ----------

constructor_df.printSchema()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Drop the unwanted columns from the dataframe

# COMMAND ----------

constructor_dropped_df = constructor_df.drop('url')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-3 Rename the column and add ingestion date

# COMMAND ----------

from pyspark.sql.functions import current_timestamp

# COMMAND ----------

constructor_final_df = constructor_dropped_df.withColumnRenamed("constructorId","constructor_id").withColumnRenamed("constructorRef","constructor_ref").withColumn("ingestion_date",current_timestamp())

# COMMAND ----------

constructor_final_df.display()

# COMMAND ----------

# MAGIC %md
# MAGIC ###write data to parquet file

# COMMAND ----------

constructor_final_df.write.mode("overwrite").parquet("/mnt/bwtformulaone1/processed/constructors")

# COMMAND ----------

# MAGIC %fs
# MAGIC ls /mnt/bwtformulaone1/processed/constructors

# COMMAND ----------



# Databricks notebook source
# MAGIC %md
# MAGIC ### Step-1 Read the drivers.json file

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, IntegerType, StringType,DateType

# COMMAND ----------

name_schema = StructType(fields=[StructField("forename",StringType(),True),
                                 StructField("surname",StringType(),True),


])

# COMMAND ----------

drivers_schema = StructType(fields=[StructField("driverId",IntegerType(),False),
                                   StructField("driverRef",StringType(),True),
                                   StructField("number",IntegerType(),True),
                                   StructField("code",IntegerType(),True),
                                   StructField("name",name_schema),
                                   StructField("dob",DateType(),True),
                                   StructField("nationality",StringType(),True),
                                   StructField("url",StringType(),True)

                                   ])

# COMMAND ----------

drivers_df = spark.read.json("/mnt/bwtformulaone1/raw/drivers.json",schema= drivers_schema)

# COMMAND ----------

drivers_df.printSchema()

# COMMAND ----------

display(drivers_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ###Rename colum and add new colum
# MAGIC 1.columname
# MAGIC 2.ingestion date
# MAGIC 3concanite name and surname

# COMMAND ----------

from pyspark.sql.functions import col,concat,current_timestamp,lit

# COMMAND ----------

drivers_with_colum_df = drivers_df.withColumnRenamed("driverId","driver_id").withColumnRenamed("driverRef","driver_ref").withColumn("ingestion_date", current_timestamp()).withColumn("name",concat(col("name.forename"),lit(" "), col("name.surname")))

# COMMAND ----------

display(drivers_with_colum_df)

# COMMAND ----------

display(drivers_final_df)

# COMMAND ----------

drivers_final_df.write.mode("overwrite").parquet("/mnt/bwtformulaone1/procr")

# COMMAND ----------

display(spark.read.parquet("/mnt/bwtformulaone1/processed/drivers"))

# COMMAND ----------



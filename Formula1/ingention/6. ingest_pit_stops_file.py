# Databricks notebook source
# MAGIC %md
# MAGIC ### Step-1 Read the pitstop.json file

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, IntegerType, StringType,DateType,FloatType

# COMMAND ----------

pit_stops_schema = StructType(fields=[StructField("raceId",IntegerType(),False),
                                   StructField("driverId",IntegerType(),True),
                                   StructField("stop",StringType(),True),
                                   StructField("lap",IntegerType(),True),
                                   StructField("time",StringType(),True),
                                   StructField("duration",StringType(),True),
                                   StructField("milliseconds",IntegerType(),True)


                                   ])

# COMMAND ----------

pit_stops_df = spark.read.json("/mnt/bwtformulaone1/raw/pit_stops.json",schema = pit_stops_schema,multiLine=True)

# COMMAND ----------

pit_stops_df.printSchema()

# COMMAND ----------

display(pit_stops_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ###Rename colum and add new colum
# MAGIC 1.columname
# MAGIC 2.ingestion date
# MAGIC 3concanite name and surname

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,col

# COMMAND ----------

final_pit_stops_df = pit_stops_df.withColumnRenamed("driverId","driver_id") \
                                  .withColumnRenamed("raceId","race_id") \
                                  .withColumn("ingestion_date", current_timestamp()) 


# COMMAND ----------

display(final_pit_stops_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ##write output to processed container in parquet format

# COMMAND ----------

final_pit_stops_df.write.mode("overwrite").parquet("/mnt/bwtformulaone1/processed/pit_stops")

# COMMAND ----------

display(spark.read.parquet("/mnt/bwtformulaone1/processed/pit_stops"))

# COMMAND ----------



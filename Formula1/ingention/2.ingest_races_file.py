# Databricks notebook source
# MAGIC %md
# MAGIC ###ingest races.csv file

# COMMAND ----------

# MAGIC %md
# MAGIC ### step-1read the csv file using the spark dataframe reader

# COMMAND ----------

from pyspark.sql.types import StringType,StructField,StructType,IntegerType,DoubleType,DateType

# COMMAND ----------

races_schema = StructType(fields=[StructField("raceId",IntegerType(), False),
                                     StructField("year",IntegerType(), True),
                                     StructField("round",IntegerType(), True),
                                     StructField("circuitId",IntegerType(), True),
                                     StructField("name",StringType(), True),
                                     StructField("date",DateType(), True),
                                     StructField("time",StringType(), True),
                                     StructField("url",StringType(), True)])                                  

# COMMAND ----------

races_df = spark.read.csv("dbfs:/mnt/bwtformulaone1/raw/races.csv",header=True,schema=races_schema )

# COMMAND ----------

display(races_df)

# COMMAND ----------

races_df.printSchema()

# COMMAND ----------

display(dbutils.fs.mounts())

# COMMAND ----------

# MAGIC %md
# MAGIC ###step-2 adding ingestion date and racetimestamp colum

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,to_timestamp,concat,col,lit

# COMMAND ----------

races_with_timestamp_df = races_df.withColumn("ingestion_date", current_timestamp()) \
        .withColumn("race_timestamp",to_timestamp(concat(col('date'), lit(' '),col('time')),'yyyy-MM-dd HH:mm:ss'))

# COMMAND ----------

display(races_with_timestamp_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step-3 select colum required rename the colum required

# COMMAND ----------

races_selected_df =races_with_timestamp_df.select(col('raceId').alias('race_id'),col('year').alias('race_year'),col('round'),col('circuitId').alias('circuit_id'),col('name'),col('ingestion_date'),col('race_timestamp'))

# COMMAND ----------

display(races_selected_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### stpe-5 write Data to Datalake as Parquet

# COMMAND ----------

races_selected_df.write.mode("overwrite").parquet("/mnt/bwtformulaone1/processed/races")

# COMMAND ----------

df = spark.read.parquet("/mnt/bwtformulaone1/processed/races")

# COMMAND ----------

display(df)

# COMMAND ----------

# MAGIC %md
# MAGIC ###write the output to procesed container in parquet format partition by year

# COMMAND ----------

races_selected_df.write.mode('overwrite').partitionBy('race_year').parquet("/mnt/bwtformulaone1/processed/races")

# COMMAND ----------

display(spark.read.parquet("/mnt/bwtformulaone1/processed/races"))

# Databricks notebook source
# MAGIC %md
# MAGIC ### Step-1 Read the result.json file

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, IntegerType, StringType,DateType,FloatType

# COMMAND ----------

results_schema = StructType(fields=[StructField("resultId",IntegerType(),False),
                                   StructField("raceId",IntegerType(),True),
                                   StructField("driverId",IntegerType(),True),
                                   StructField("constructorId",IntegerType(),True),
                                   StructField("number",IntegerType(),True),
                                   StructField("grid",IntegerType(),True),
                                   StructField("position",IntegerType(),True),
                                   StructField("positionText",StringType(),True),
                                   StructField("positionOrder",IntegerType(),True),
                                   StructField("points",FloatType(),True),                                     StructField("laps",IntegerType(),True),
                                   StructField("time",StringType(),True),
                                   StructField("milliseconds",IntegerType(),True),
                                   StructField("fastestLap",IntegerType(),True),
                                   StructField("rank",IntegerType(),True),
                                   StructField("fastestLapTime",StringType(),True), 
                                   StructField("fastestLapSpeed",FloatType(),True),
                                   StructField("statusId",StringType(),True),


                                   ])

# COMMAND ----------

results_df = spark.read.json("/mnt/bwtformulaone1/raw/results.json",schema= results_schema)

# COMMAND ----------

results_df.printSchema()

# COMMAND ----------

display(results_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ###Rename colum and add new colum
# MAGIC 1.columname
# MAGIC 2.ingestion date
# MAGIC 3concanite name and surname

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,col

# COMMAND ----------

results_with_colum_df = results_df.withColumnRenamed("driverId","driver_id") \
                                  .withColumnRenamed("resultId","result_id") \
                                  .withColumnRenamed("constructorId","constructor_id") \
                                  .withColumnRenamed("raceId","race_id") \
                                  .withColumnRenamed("positionText","position_text") \
                                  .withColumnRenamed("positionOrder","position_order") \
                                  .withColumnRenamed("fastestLap","fastest_lap") \
                                  .withColumnRenamed("fastestLapTime","fastest_lap_time") \
                                  .withColumnRenamed("fastestLapSpeed","fastest_lap_speed") \
                                  .withColumn("ingestion_date", current_timestamp()) 


# COMMAND ----------

display(results_with_colum_df)

# COMMAND ----------

results_final_df = results_with_colum_df.drop(col("statusId"))

# COMMAND ----------

# MAGIC %md
# MAGIC ##write output to processed container in parquet format

# COMMAND ----------

results_final_df.write.mode("overwrite").partitionBy("race_id").parquet("/mnt/bwtformulaone1/processed/results")

# COMMAND ----------

display(spark.read.parquet("/mnt/bwtformulaone1/processed/results"))

# COMMAND ----------



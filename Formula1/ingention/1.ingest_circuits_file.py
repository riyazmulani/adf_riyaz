# Databricks notebook source
# MAGIC %md
# MAGIC ###ingest circuits.csv file

# COMMAND ----------

dbutils.widgets.help()

# COMMAND ----------

dbutils.widgets.text("p_data_source", "")
v_data_source =dbutils.widgets.get("p_data_source")

# COMMAND ----------

v_data_source

# COMMAND ----------

# MAGIC %md
# MAGIC ### step-1read the csv file using the spark dataframe reader

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_functions"

# COMMAND ----------

raw_folder_path

# COMMAND ----------

from pyspark.sql.types import StringType,StructField,StructType,IntegerType,DoubleType

# COMMAND ----------

circuits_schema = StructType(fields=[StructField("circuitId",IntegerType(), False),
                                     StructField("circuitRef",StringType(), True),
                                     StructField("name",StringType(), True),
                                     StructField("location",StringType(), True),
                                     StructField("country",StringType(), True),
                                     StructField("lat",DoubleType(), True),
                                     StructField("lng",DoubleType(), True),
                                     StructField("alt",IntegerType(), True),
                                     StructField("url",StringType(), True)])                                  

# COMMAND ----------

circuits_df = spark.read.csv(f"{raw_folder_path}/circuits.csv",header=True,schema=circuits_schema )

# COMMAND ----------

display(circuits_df)

# COMMAND ----------

circuits_df.printSchema()

# COMMAND ----------

display(dbutils.fs.mounts())

# COMMAND ----------

# MAGIC %fs
# MAGIC ls /mnt/bwtformulaone1/raw

# COMMAND ----------

# MAGIC %md
# MAGIC ###step-2 select only required column

# COMMAND ----------

circuits_selected_df = circuits_df.select("circuitId","circuitRef","name","location","country","lat","lng","alt")

# COMMAND ----------

circuits_selected_df = circuits_df.select(circuits_df.circuitId,circuits_df.circuitRef,circuits_df.name,circuits_df.location,circuits_df.country,circuits_df.lat,circuits_df.lng,circuits_df.alt)

# COMMAND ----------

display(circuits_selected_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step-3 Rename the columns as required

# COMMAND ----------

from pyspark.sql.functions import lit

# COMMAND ----------

circuits_renamed_df = circuits_selected_df.withColumnRenamed("circuitId","circuit_id") \
                                          .withColumnRenamed("circuitRef","circuit_ref") \
                                          .withColumnRenamed("lat","lattitude") \
                                          .withColumnRenamed("lng","longitude") \
                                          .withColumnRenamed("alt","altitude") \
                                          .withColumn("data_source",lit(v_data_source))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-4 Add ingetion date to Dataframe

# COMMAND ----------

from pyspark.sql.functions import current_timestamp

# COMMAND ----------

circuits_final_df = add_ingestion_date(circuits_renamed_df)

# COMMAND ----------

display(circuits_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### stpe-5 write Data to Datalake as Parquet

# COMMAND ----------

circuits_final_df.write.mode("overwrite").parquet(f"{processed_folder_path}/circuits")

# COMMAND ----------

df = spark.read.parquet("/mnt/bwtformulaone1/processed/circuits")

# COMMAND ----------

display(df)

# COMMAND ----------


